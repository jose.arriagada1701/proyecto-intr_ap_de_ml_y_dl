# Proyecto de INTRODUCCIÓN A LAS APLICACIONES DE ALGORITMOS DE MACHINE LEARNING Y DEEP LEARNING
## Por José Arriagada - 2023-02

Este proyecto se centra en el desarrollo de una herramienta predictiva para el ajedrez, utilizando modelos de red neuronal recurrente (RNN) implementados en TensorFlow. Los datos fueron recopilados mediante técnicas de web scraping desde la página [Chess Poster](https://www.chess-poster.com/), seguidos de un procesamiento que resultó en la creación de tres modelos RNN únicos. Aunque la precisión de estos modelos se situó en un 23%, el proyecto identifica áreas potenciales de mejora, especialmente en la abordación de las limitaciones inherentes a los datos.

Se destaca la importancia de este trabajo como un paso inicial para investigaciones futuras sobre la interpretación de datos secuenciales en el ajedrez y su aplicación en la predicción de resultados. Además, se hace referencia a trabajos previos que exploran el uso de redes neuronales convolucionales (CNN) y datos secuenciales en el ámbito del ajedrez.

![www.chess-poster.com](https://www.chess-poster.com/images/mailogo.gif)

El reporte completo del proyecto se encuentra disponible [aqui](https://gitlab.com/jose.arriagada1701/proyecto-intr_ap_de_ml_y_dl/-/blob/main/Reporte/reporte.pdf?ref_type=heads)

