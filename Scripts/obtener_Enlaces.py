import requests
from bs4 import BeautifulSoup
import re
from urllib.parse import urljoin

def obtener_enlaces_zip(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
    
    # Realizar solicitud HTTP con el encabezado User-Agent
    response = requests.get(url, headers=headers)
    
    # Verificar si la solicitud fue exitosa (código de estado 200)
    if response.status_code == 200:
        # Analizar el contenido HTML de la página
        soup = BeautifulSoup(response.text, 'html.parser')
        
        # Encontrar todos los enlaces que contienen la cadena ".zip"
        enlaces_zip = soup.find_all('a', href=re.compile('.zip'))
        
        # Obtener los enlaces absolutos
        enlaces_absolutos = [urljoin("https://www.chess-poster.com/", enlace['href']) for enlace in enlaces_zip]
        
        return enlaces_absolutos
    else:
        # Si la solicitud no fue exitosa, imprimir el código de estado
        print(f"Error al obtener la página. Código de estado: {response.status_code}")
        return None

def guardar_enlaces_en_txt(enlaces, nombre_archivo='enlaces_zip.txt'):
    with open(nombre_archivo, 'w') as archivo:
        for enlace in enlaces:
            archivo.write(enlace + '\n')
    print(f"Enlaces guardados en {nombre_archivo}")

# URL de la página principal
url_principal = "https://www.chess-poster.com/spanish/pgn_e/archivos_pgn.htm#K"

# Obtener los enlaces de los archivos zip
enlaces_zip = obtener_enlaces_zip(url_principal)

# Imprimir los enlaces obtenidos
if enlaces_zip:
    for enlace in enlaces_zip:
        print(enlace)

    # Guardar los enlaces en un archivo de texto
    guardar_enlaces_en_txt(enlaces_zip)

