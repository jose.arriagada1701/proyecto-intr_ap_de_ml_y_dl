import subprocess
import time
import os

# Crear la carpeta "player" si no existe
carpeta_destino = "player"
if not os.path.exists(carpeta_destino):
    os.makedirs(carpeta_destino)

# Leer los enlaces desde el archivo txt.zip.txt
with open("enlaces_zip.txt", "r") as archivo:
    enlaces = archivo.read().splitlines()

for enlace in enlaces:
    # Extraer el nombre del archivo ZIP desde el enlace
    nombre_archivo = enlace.split("/")[-1]
    
    # Construir la ruta de destino dentro de la carpeta "player"
    ruta_destino = os.path.join(carpeta_destino, nombre_archivo)
    
    # Construir el comando wget con el enlace actual y la ruta de destino
    comando_wget = ["wget", enlace, "-P", carpeta_destino]
    
    # Ejecutar el comando wget
    subprocess.run(comando_wget)

    # Esperar un segundo antes de la siguiente descarga
    time.sleep(1)

