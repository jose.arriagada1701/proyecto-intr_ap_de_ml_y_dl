import os
import chess
import chess.pgn
import csv

def pgn_to_csv_folder(pgn_folder, csv_file):
    """
    Concatena información de todas las partidas en archivos PGN en una carpeta y las escribe en un archivo CSV.

    Args:
        pgn_folder: La ruta de la carpeta que contiene los archivos PGN.
        csv_file: El nombre del archivo CSV de salida.

    Returns:
        None.
    """
    header_written = False

    with open(csv_file, "w", newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        for filename in os.listdir(pgn_folder):
            if filename.endswith(".pgn"):
                pgn_file_path = os.path.join(pgn_folder, filename)

                with open(pgn_file_path, "r") as pgn_file:
                    pgn_game = chess.pgn.read_game(pgn_file)

                    while pgn_game:
                        if not header_written:
                            # Escribe el encabezado CSV solo una vez
                            header = ["Event", "Site", "Date", "Round", "White", "Black", "Result", "WhiteElo", "BlackElo", "ECO", "Moves"]
                            csv_writer.writerow(header)
                            header_written = True

                        data = {key: "" for key in header}

                        for key in data.keys():
                            node = pgn_game.headers.get(key)
                            if node:
                                data[key] = node.strip()

                        board = pgn_game.board()
                        moves = []
                        for move in pgn_game.mainline_moves():
                            moves.append(board.san(move))
                            board.push(move)
                        data["Moves"] = " ".join(moves)

                        csv_writer.writerow(data.values())

                        pgn_game = chess.pgn.read_game(pgn_file)

def pgn_to_csv(pgn_file, csv_file):
    """
    Extrae información de todas las partidas de un archivo PGN y las escribe a un archivo CSV.

    Args:
        pgn_file: El nombre del archivo PGN de entrada.
        csv_file: El nombre del archivo CSV de salida.

    Returns:
        None.
    """
    with open(pgn_file, "r") as pgn_file:
        # Utiliza la biblioteca chess.pgn para analizar el archivo PGN
        pgn_game = chess.pgn.read_game(pgn_file)

        # Abre el archivo CSV para escritura
        with open(csv_file, "w", newline='') as csv_file:
            csv_writer = csv.writer(csv_file)

            # Escribe el encabezado CSV
            header = ["Event", "Site", "Date", "Round", "White", "Black", "Result", "WhiteElo", "BlackElo", "ECO", "Moves"]
            csv_writer.writerow(header)

            # Recorre todas las partidas en el archivo PGN
            while pgn_game:
                # Inicializa los datos para cada partida
                data = {key: "" for key in header}

                # Llena los datos con la información de la partida actual
                for key in data.keys():
                    node = pgn_game.headers.get(key)
                    if node:
                        data[key] = node.strip()

                # Obtiene las jugadas de la partida actual
                board = pgn_game.board()
                moves = []
                for move in pgn_game.mainline_moves():
                    moves.append(board.san(move))
                    board.push(move)
                data["Moves"] = " ".join(moves)

                # Escribe la fila CSV para la partida actual
                csv_writer.writerow(data.values())

                # Lee la siguiente partida en el PGN
                pgn_game = chess.pgn.read_game(pgn_file)

if __name__ == "__main__":
    pgn_folder = "PGN"
    csv_file_folder = "concatenated_games_folder.csv"
    pgn_to_csv_folder(pgn_folder, csv_file_folder)

    pgn_file = "concatenated_games.pgn"
    csv_file = "concatenated_games.csv"
    pgn_to_csv(pgn_file, csv_file)


