# Utilizar la imagen oficial de Python 3.8
FROM python:3.8

# Instalar las dependencias necesarias
RUN apt-get update && apt-get install -y \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

# Establecer el directorio de trabajo
WORKDIR /app

# Copiar la carpeta modelos y el archivo data_set.csv al contenedor
COPY modelos /app/modelos
COPY data_set.csv /app/

# Instalar las bibliotecas necesarias
RUN pip install pandas tensorflow

# Copiar el script de Python al contenedor
COPY modelos.py /app/

# Ejecutar el script
CMD ["python", "modelos.py"]

