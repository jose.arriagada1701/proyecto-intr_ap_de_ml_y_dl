from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences 

# Cargar los modelos desde /app/modelos (ruta ajustada)
model1 = load_model('/app/modelos/modelo1.h5')
model2 = load_model('/app/modelos/modelo2-30epochs_185708.h5')
model3 = load_model('/app/modelos/modelo3.h5')
#model4 = load_model('/app/modelos/modelo4-30epochs_192540.h5')

# Crear un objeto Tokenizer y ajustarlo a los datos existentes
tokenizer = Tokenizer()  # Instancia del Tokenizer
tokenizer.fit_on_texts([])  # Ajusta el Tokenizer según tus datos de entrenamiento

# Hacer predicciones en nuevos datos
new_moves = ["e4 e5 Nf3 Nf6 d4 exd4 e5 Ne4 Qxd4 d5 exd6 Nxd6 Bd3 Nc6 Qf4 Be7 O-O O-O Nc3 Be6 Be3 Qd7 Rad1 Rad8 Ng5 Bxg5 Qxg5 f6 Qh4 Bf5 Bc5 Bxd3 Rxd3 Qe6 Nd5 Rf7 Rfd1 Rfd7 Nxc7 Rxc7 Bxd6 Rcd7 Qg3 Qxa2 h4 Qe6 h5 h6 b3 Ne5 Bxe5 Rxd3 Rxd3 Rxd3 Qxd3 fxe5 Qd8+ Kh7 Qd3+ Kh8 Qd8+"]
new_sequences = tokenizer.texts_to_sequences(new_moves)
new_padded_sequences = pad_sequences(new_sequences, maxlen=1498)

# Realizar predicciones en los 4 modelos
predictions_model1 = model1.predict(new_padded_sequences)
predictions_model2 = model2.predict(new_padded_sequences)
predictions_model3 = model3.predict(new_padded_sequences)
#predictions_model4 = model4.predict(new_padded_sequences)

# Convertir las salidas del modelo en etiquetas ("1-0", "0-1", "1/2-1/2")
def convert_to_result_label(predictions):
    if predictions == 0:
        return "0-1"
    elif predictions ==1:
        return "1-0"
    else:
        return "1/2-1/2"

# Obtener las etiquetas de los resultados para cada modelo
result_label_model1 = convert_to_result_label(predictions_model1)
result_label_model2 = convert_to_result_label(predictions_model2)
result_label_model3 = convert_to_result_label(predictions_model3)
#result_label_model4 = convert_to_result_label(predictions_model4)

# Puedes usar las etiquetas según tus necesidades
print("Es 1/2-1/2")
print("Resultado del Modelo 1:", result_label_model1)
print("Resultado del Modelo 2:", result_label_model2)
print("Resultado del Modelo 3:", result_label_model3)
#print("Resultado del Modelo 4:", result_label_model4)

new_moves = ["e4 e5 Nf3 Nc6 Bb5 a6 Ba4 Nf6 d3 d6 c3 Be7 Nbd2 b5 Bc2 O-O Nf1 d5 Qe2 Re8 Ng3 h6 O-O Be6 d4 dxe4 Nxe5 Bd5 Nxe4 Bd6 f4 Nxe4 Bxe4 Bf8 Bxd5 Qxd5 Qf3 Qxf3 Rxf3 Ne7 f5 f6 Nd3 Nd5 Nf4 Nxf4 Bxf4 Bd6 Bxd6 cxd6 Kf2 b4 Rc1 Rab8 c4 Re4 Rd3 Rf4+ Ke2 Rxf5 c5 dxc5 dxc5 Re5+ Re3 Rxe3+ Kxe3 Kf7 Kd4 Ke6 Re1+ Kd7 Kd5 Rb5 Re4 g6 h4 f5 Rf4 h5 Rd4 Kc7 b3 Rb8 Kc4 Re8 Rd6 Re4+ Kd5 Rxh4 Rxg6 Rg4 Rxa6 Rxg2 Ra7+ Kb8 Rh7 Rxa2 Rxh5 Rc2 Kc6 Ka7 Kb5 Re2 Rh7+ Kb8 Kb6 Re8 c6 f4 Rb7+ Kc8 Ra7"]

new_sequences = tokenizer.texts_to_sequences(new_moves)
new_padded_sequences = pad_sequences(new_sequences, maxlen=1498)

# Realizar predicciones en los 4 modelos
predictions_model1 = model1.predict(new_padded_sequences)
predictions_model2 = model2.predict(new_padded_sequences)
predictions_model3 = model3.predict(new_padded_sequences)
#predictions_model4 = model4.predict(new_padded_sequences)

# Convertir las salidas del modelo en etiquetas ("1-0", "0-1", "1/2-1/2")
def convert_to_result_label(predictions):
    if predictions == 0:
        return "0-1"
    elif predictions ==1:
        return "1-0"
    else:
        return "1/2-1/2"

# Obtener las etiquetas de los resultados para cada modelo
result_label_model1 = convert_to_result_label(predictions_model1)
result_label_model2 = convert_to_result_label(predictions_model2)
result_label_model3 = convert_to_result_label(predictions_model3)
#result_label_model4 = convert_to_result_label(predictions_model4)

# Puedes usar las etiquetas según tus necesidades
print("Es 1-0")

print("Resultado del Modelo 1:", result_label_model1)
print("Resultado del Modelo 2:", result_label_model2)
print("Resultado del Modelo 3:", result_label_model3)
#print("Resultado del Modelo 4:", result_label_model4)
